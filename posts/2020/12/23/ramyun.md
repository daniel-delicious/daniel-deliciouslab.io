---
title: Ramyun night
tags: ramyun
---

# Ramyun Night

Built on a [Shin Ramyun Black](https://www.amazon.com/gp/product/B017IRZLKQ/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B017IRZLKQ&linkCode=as2&tag=danramteke-20&linkId=6eaf0092ceddada2ca4184736d7c63a2). <img src="//ir-na.amazon-adsystem.com/e/ir?t=danramteke-20&l=am2&o=1&a=B017IRZLKQ" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important; width: 1px; height: 1px;" />

![Ramyun photo](./ramyun.png)




## Preparation

I sliced 2 scallions, half of a handful of coriander, and 1 clove of garlic. Half of an old serrano - the other half didn't look so good.

I heated a separate pan, and fried some pre-cut cold pepperonies. Like five seconds per side.

I sliced a quarter of a zucchini into slices.

## Cooking


I heated 2 tsp ghee in a saucepan over a low heat.

Once it was hot, I added the sliced scallions, stirring to make sure they don't char. 

Two minutes later, I added the the coriander, garlic, and half of a serranno chili.

I let that sweat for a couple minutes and then added 3 cups of water. 

I added the zucchini slices too.

I covered it so that it would come up to boil quicker.

Once it was about to boil, I added the flavor packets from the ramyun.

Added the noodles, without stirring, and set the clock to 4 minutes.

Once the timer read two minutes, I cracked an egg into the pot. It nestled on top of the floating pod of unstirred noodles.

Once the timer dings, I turn the heat off right away.

Empty into bowl, top with the fried pepperonies from the beginning.

## Eat

So delicious. The garlic and coriander with ghee are the perfect net for all the other flavors. Bursts of greasy pepperoni. The egg was a little over cooked, probably because I ate it last.

Drank two cans of [Guinness](https://www.mercato.com/item/guinness-beer-draught-stout-4-pack-149-fluid-ounces/52046) with it. 

## Tweaks

The zucchini strips were a little over cooked as well. I think next time I'll put them in right before the noodles, instead of before boiling the water. That way, they'll be more of a crisp ingredient instead of flavoring the broth.

The egg was maybe a bit smaller than usual. I wonder if I could compensate for egg size by putting it in 30 seconds later.
