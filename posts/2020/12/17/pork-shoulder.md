---
title: Pork shoulder
---

# Pork Shoulder

Started with an 8.6 pound pork shoulder. Seasoned it, cooked it, and then ate it.

## Seasoning 

The night before, ground up one tablespoon each of cumin seeds, coriander seeds, methi seeds, kolonji seeds, mustard seeds, fennel seeds, 3 brown cardamoms, 5 cloves, and 2 star anise. 

![Spices Whole](images/spices-whole.png) ![Spices Ground](images/spices-ground.png)

Covered the pork shoulder in 2 tablespoons of salt. 

![Shoulder Salted](images/shoulder-salted.png)


And then covered the pork shoulder in the ground up spices. 

![Shoulder Spiced](images/shoulder-spiced.png)

In the fridge at 5:45 PM. 

## Cooking

**9:30 AM** took it out of the fridge. Sat on counter to come down closer to room temperature.

![Shoulder out of fridge](images/shoulder-out-of-fridge.png)

**10:30 AM** put it in the oven at 200F. Low and slow.

![Shoulder in oven](images/shoulder-in-oven-1030.png)

**1:30 PM** checked shoulder

![Shoulder in oven](images/shoulder-in-oven-1330.png)


**6:00 PM** Checked shoulder

![Shoulder in oven](images/shoulder-in-oven-1800.png)


## Eating

**8:00 PM**, pulled shoulder out of oven

![Shoulder done](images/pork-shoulder-2000.png)

On a serving plate for the table.

![Pork on plate](images/pork-on-plate.png)


On a tortilla for eating.

![Pork on tortilla](images/pork-on-tortilla.png)

I love it. I don't have to wonder if the pork is done or not. A fork slides in so easily. And the meat instantly separates from the bone. The pork tastes so juicy, so delicious. For the little amount of effort, this dish is so tasty.