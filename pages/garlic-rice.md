---
title: Garlic Rice
category: recipes
---
# Garlic Rice

**First things first** Measure the rice, and start soaking it while prepping everything else.

## Implements

- **1** saucepan with lid
- **1** frying pan
- **1** strainer, or something to help drain the rice after soaking
- *Optional* **1** blender or mortar

## Ingredients

Here are the ingredients

#### Sauce Pan 
- **3/4 cup** of rice, covered in water to soak


#### Prep bowl 1
- **1 tablespoon** of ghee

#### Prep Bowl 2
- **1/2** onion, chopped up

#### Prep Bowl 3
- **1/4 cup** of coriander, chopped up (also known as dhania or cilantro)
- **2 cloves** of garlic, crushed into a paste. Or 2 teaspoons of garlic paste

#### Prep Bowl 4
- **1/2** teaspoon of turmeric


## Preparation

- **Drain** the soaked rice
- **Add** 1.5 cups of water
- **Add** salt
- **Heat** to a boil, and then quickly
- **Cover** and
- **Reduce heat** to very low. If there's steam coming out under the lid, the flame is too high. Turn it down so that if you turn it up just a little bit, steam starts coming out
- **15 mintues** later, turn off the heat, and fluff the rice. While the rice is cooking, start the sauce.
- **Heat** the ghee in the frying pan over low heat until it runs clear. This will take about a minute.
- **Add** the onions, and cook for about 5 minutes.
- **Add** coriander and garlic and cook for about 3 minutes
- **Add** the turmeric and mix
- **Add** the cooked and fluffed rice to the frying pan, and mix well.
- **2 minutes** later, the rice is ready to eat.


## Eating

Eat it plain or with yoghurt, or under curry. Can also serve along side plain rice.
