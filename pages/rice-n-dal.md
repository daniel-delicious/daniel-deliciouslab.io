---
title: Rice and Dal
category: recipes
---
# Rice & Dal

**First things first.** Measure the rice and the dal, and start soaking them while gathering everything else.

## Implements

Here are the equipment needed.

- **3** saucepans with their lids. (at least 1 should be medium size, other two can be small.)
- **1** wet measuring cup for water
- **1** dry measuring cup for rice and dal
- **2** teaspoons for scooping spices & pastes
- **strainer** or something to help rinse the rice/dal.

## Ingredients

Here are the ingredients. Each prep bowl goes in to the pot at the same time. 

#### Sauce pan 1 

- **3/4** cup rice covered in water to soak

#### Sauce pan 2 

- **3/4** cup mysore dal, covered in water to soak. Mysore dal is also known as red dal.


#### Prep Bowl 1 

- **2** tablespoons of ghee

#### Prep Bowl 2 

- **1** teaspoon of cumin seeds
- **1** teaspoon of coriander seeds
- **1** teaspoon of mustard seeds
- **1** teaspoon of black peppercorns
- **1** cinnamon stick
- **2** brown cardamom
- **3** cloves
- **1** bay leaf


#### Prep Bowl 3

- **1** chopped up red onion
- **1** teaspoon of salt


#### Prep Bowl 4

- **1** teaspoon garlic paste 
- **1** teaspoon ginger paste. 

*Can of course use fresh garlic & garlic, smash them into a paste, adding 
half a teaspoon of water to lubricate if needed.*

#### Prep Bowl 5

- tablespoon of tomato paste

#### Prep Bowl 6 

- **1** teaspoon turmeric 
- **1** teaspoon chili powder. Adjust to match your taste. 

#### Prep Bowl 7 

- **2** cardamom

#### Prep Bowl 8

- **1** teaspoon of ghee

## Preparation

Low and slow.

**Drain** the dal in saucepan 2, and put the dal back in the pot. 

**Add** 1.5 cups of water and a teaspoon of salt. 

**Heat** to a boil, and then **cover** with a lid, **reduce** heat to very low. If there's steam coming out under the lid, the flame is too high.

**20 minutes** later, turn the heat off, and **stir** the dal.

**Heat** the ghee (Prep bowl 1) on lowheat in saucepan 3, the largest pot, till the ghee runs clear. This will probably be only a minute or two.

**Add** in spices from (Prep bowl 2). The aroma will slowly get more and more fragrant. Once the cinnomon unfurls, go to the next step. This will probably be 2 to 10 minutes.

**Add** in the onion. Cook for about 10 minutes or until the onion starts to get translucent. Stir occaisionally. 

**Add** in the ginger and garlic paste (prep bowl 4), cook for about a minute

**Add** in the turmeric and chili (prep bowl 5),

**Add** in tomato paste. Stir in cooked dal. 

**Lower** heat and cover. Keep it simmering till the rice is done. The dal can simmer for a long time, but at least 10 minutes.

**10 minutes** before eating, turn the heat to to let the dal calm down. You know how stew is always better the next day? Same idea, but 10 minute minimum.


- **Drain** the rice
- **Add** 1.5 cups of water
- **Add** salt
- **Add** cardamon (Prep Bowl 7) and ghee (Prep Bowl 8)
- **Heat** to a boil, and then quickly
- **Cover** and
- **Reduce heat** to very low. If there's steam coming out under the lid, the flame is too high. Turn it down so that if you turn it up just a little bit, steam starts coming out
- **15 mintues** later, turn off the heat, and fluff the rice.

- **Plate** the rice with the dal on top, with sides of yogurt and mango pickle.  If there's lefovers, top with a poached egg for breakfast.